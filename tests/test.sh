#!/bin/sh -eu

IMAGE_TAG=${1:?}

cd tests/pq_build_test

docker run -t --rm \
	-v $PWD:/workdir \
	registry.gitlab.com/rust_musl_docker/image:$IMAGE_TAG \
	cargo build --release -vv --target=x86_64-unknown-linux-musl

cd ../..

cd tests/pq_ssl_build_test

docker run -t --rm \
	-v $PWD:/workdir \
	registry.gitlab.com/rust_musl_docker/image:$IMAGE_TAG \
	cargo build --release -vv --target=x86_64-unknown-linux-musl

cd ../..
